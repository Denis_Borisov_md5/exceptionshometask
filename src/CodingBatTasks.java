public class CodingBatTasks
{
    //4 задачи из модуля АР-1
    //scoresIncreasing
    //scores100
    //wordsCount
    //wordsFront
    public boolean scoresIncreasing(int[] scores)
    {
        for ( int i = 0; i < scores.length - 1; i++ )
        {
            if ( scores[i + 1] < scores[i] )
            {
                return false;
            }
        }
        return true;
    }
    public static boolean scores100( int[] scores )
    {
        for ( int i = 0; i < scores.length - 1; i++ )
        {
            if ( scores[i] == 100 && scores[i + 1] == 100 )
            {
                return true;
            }
        }

        return false;
    }
    public static int wordsCount( String[] words, int len )
    {
        int count = 0;

        for ( String s : words )
        {
            if ( s.length() == len )
            {
                count++;
            }
        }

        return count;
    }
    public static String[] wordsFront( String[] words, int n )
    {
        String[] result = new String[n];

        for ( int i = 0; i < n; i++ )
        {
            result[i] = words[i];
        }

        return result;
    }
}

